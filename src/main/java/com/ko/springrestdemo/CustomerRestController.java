package com.ko.springrestdemo;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ko.springdemo.entity.Customer;

@RestController
@RequestMapping("/api")
public class CustomerRestController {

	private List<Customer> theCustomers;

	// define @PostConstruct to load the customer data ... only once!
	@PostConstruct
	public void loadData() {

		theCustomers = new ArrayList<>();

		theCustomers.add(new Customer("David", "Beckham"));
		theCustomers.add(new Customer("David", "Alaba"));
		theCustomers.add(new Customer("David", "Goliath"));

	}

	// define endpoint for "/customer" - return list of customers

	@GetMapping("/customers")
	public List<Customer> getCustomeres() {

		return theCustomers;
	}

	// define endpoint for "/customers/{customerId}"
	@GetMapping("/customers/{customerId}")
	public Customer getCustomer(@PathVariable int customerId) {

		// just index into the list ... keeping it simple for now

		// check the customerId against list size
		if (customerId >= theCustomers.size() || (customerId < 0)) {
			throw new CustomerNotFoundException("Customer id not found" + customerId);
		}

		return theCustomers.get(customerId);
	}

}
